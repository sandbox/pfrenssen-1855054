<?php

/**
 * Base class used for writing atomic remote tests.
 *
 * The test cases are written in such a way that they can be run against a
 * staging or live environment and will clean up after themselves by reverting
 * all peformed actions. The tests should be performed through a set of actions
 * defined in the test method. For example:
 * <code>
 * protected function testFoo() {
 *   $this->actions[] = 'login';
 * }
 *
 * protected function performLogin();
 * protected function revertLogin();
 * </code>
 *
 * The setUp() and tearDown() methods will not create an environment, but
 * instead direct the internal browser to the remote URL,
 * 'simpletest_remote_url'. The tests will be run against the specified URL
 * instead of the local machine.
 *
 * The test can also be run against the local development environment by
 * leaving the variable blank. If not specified the remote URL will be filled
 * with the local environment's URL.
 */
class DrupalRemoteTestCase extends DrupalWebTestCase {

  /**
   * URL variables that need to be changed when running against remote server.
   *
   * @var array
   */
  protected static $URL_VARIABLES = array('base_url', 'base_secure_url', 'base_insecure_url');

  /**
   * Prefix to be added to all random strings.
   */
  protected static $REMOTE_PREFIX;

  /**
   * URL of the remote server.
   *
   * @var string
   */
  protected $remoteUrl;

  /**
   * Associative array of original values for the URL variables.
   *
   * @var array
   */
  protected $originalUrls = array();

  /**
   * List of actions to perform.
   *
   * @var array
   */
  protected $actions = array();

  /**
   * Determine when to run against remote environment.
   */
  protected function setUp() {
    // Set to that verbose mode works properly.
    $this->originalFileDirectory = variable_get('file_public_path', conf_path() . '/files');

    // Clone the current connection and replace the current prefix.
    $connection_info = Database::getConnectionInfo('default');
    Database::renameConnection('default', 'simpletest_original_default');
    foreach ($connection_info as $target => $value) {
      $connection_info[$target]['prefix'] = array(
        'default' => $value['prefix']['default'] . $this->databasePrefix,
      );
    }
    Database::addConnectionInfo('default', 'default', $connection_info['default']);

    // Store the remote url. This should not end on a trailing slash.
    if (!$this->remoteUrl && !($this->remoteUrl = variable_get('simpletest_remote_url', FALSE))) {
      $this->remoteUrl = rtrim(url('', array('absolute' => TRUE)), '/');
    }
    // Point the internal browser to the staging site.
    foreach (self::$URL_VARIABLES as $variable) {
      $this->originalUrls[$variable] = $GLOBALS[$variable];
      $GLOBALS[$variable] = $this->remoteUrl;
    }
    $GLOBALS['base_secure_url'] = str_replace('http://', 'https://', $GLOBALS['base_secure_url']);

    // Generate unique remote prefix.
    self::$REMOTE_PREFIX = 'test' . mt_rand(100, 100000);

    // Use the actual files directories.
    $this->public_files_directory = variable_get('file_public_path', conf_path() . '/files');
    $this->private_files_directory = variable_get('file_private_path', '');
    $this->temp_files_directory = variable_get('file_temporary_path', file_directory_temp());

    // Set the time-limit for the test case.
    drupal_set_time_limit($this->timeLimit);
    $this->setup = TRUE;
  }

  /**
   * Perform and revert actions, then tear down based on what setUp() did.
   */
  protected function tearDown() {
    // Perform all actions as part of the test and revert them.
    $this->performActions();
    $this->revertActions();

    // Revert all URL variables to their original values.
    foreach (self::$URL_VARIABLES as $variable) {
      $GLOBALS[$variable] = $this->originalUrls[$variable];
    }

    // BEGIN: Excerpt from DrupalUnitTestCase.
    global $conf;

    // Get back to the original connection.
    Database::removeConnection('default');
    Database::renameConnection('simpletest_original_default', 'default');

    $conf['file_public_path'] = $this->originalFileDirectory;
  }

  /**
   * Perform all actions listed in the $actions array.
   */
  protected function performActions() {
    foreach ($this->actions as $action) {
      call_user_func(array($this, 'perform' . ucfirst($action)));
    }
  }

  /**
   * Revert all actions listed in the $actions array.
   */
  protected function revertActions() {
    foreach ($this->actions as $action) {
      if (method_exists($this, 'revert' . ucfirst($action))) {
        call_user_func(array($this, 'revert' . ucfirst($action)));
      }
    }
  }

  /**
   * Set the user agent to Drupal.
   */
  protected function curlInitialize() {
    parent::curlInitialize();
    curl_setopt($this->curlHandle, CURLOPT_USERAGENT, 'Drupal (+http://drupal.org/)');
  }

  /**
   * Set the remote URL base.
   *
   * @param $url
   *   Base of the remote URL, for example: http://example.com
   */
  protected function setUrl($url) {
    $prefix = self::$REMOTE_PREFIX;
    $this->tearDown();
    $this->remoteUrl = $url;
    $this->setUp();
    self::$REMOTE_PREFIX = $prefix;
  }

  /**
   * Reset the remote URL base to the value in 'simpletest_remote_url'.
   */
  protected function resetUrl() {
    $this->setUrl(variable_get('simpletest_remote_url', FALSE));
  }

  /**
   * Override to ensure not database activity.
   */
  protected function refreshVariables() {
    // Do nothing.
  }

  /**
   * Add remote prefix.
   */
  public static function randomName($length = 8) {
    return self::$REMOTE_PREFIX . parent::randomName($length);
  }

  /**
   * Add remote prefix.
   */
  public static function randomString($length = 8) {
    return self::$REMOTE_PREFIX . parent::randomString($length);
  }

  /**
   * Temporarily revert the global URL variables so verbose links will print.
   */
  protected function verbose($message) {
    if ($this->remoteUrl) {
      foreach (self::$URL_VARIABLES as $variable) {
        $GLOBALS[$variable] = $this->originalUrls[$variable];
      }
    }

    parent::verbose($message);

    if ($this->remoteUrl) {
      foreach (self::$URL_VARIABLES as $variable) {
        $GLOBALS[$variable] = $this->remoteUrl;
      }
      $GLOBALS['base_secure_url'] = str_replace('http://', 'https://', $GLOBALS['base_secure_url']);
    }
  }
}
