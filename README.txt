This is a port of the DrupalRemoteTestCase of the original contributed
Simpletest module, extending the version of Simpletest that is now in Drupal
core.

@see http://drupal.org/project/simpletest
